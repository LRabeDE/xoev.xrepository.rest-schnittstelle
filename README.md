# XRepository: Übersicht der REST-Webservices zu Codelisten (Metadaten und Daten)
Alle dargestellten Requests und zugehörigen Responses sind für die Umgebung https://www.xrepository.de/ und beispielhaft für die Codelsite [Erreichbarkeit](https://www.xrepository.de/details/urn:de:xoev:codeliste:erreichbarkeit) beziehungsweise deren Versionen und Dateien dargestellt.

## Metadaten einer Codeliste und zugehörige Versionen
Mit diesem Abruf werden die Metadaten und zugehörigen Versionen zu einer im XRepository gehaltene Codeliste abgefragt. Der Abrufparameter ist die Kennung der Codeliste. Geliefert wird u.a. das Metadatum Versionskennung (/gc:CodeList/Identification/CanonicalVersionUri) der zur Codeliste gehaltenen Versionen im Status "veröffentlicht".

**Request:** https://www.xrepository.de/api/codeliste/urn:de:xoev:codeliste:erreichbarkeit  

**Response:**
```
<dat:Codeliste>
    <dat:nameLang>Kommunikationskanal / Erreichbarkeit</dat:nameLang>
    <dat:nameKurz>Erreichbarkeit</dat:nameKurz>
    <dat:kennung>urn:de:xoev:codeliste:erreichbarkeit</dat:kennung>
    <dat:beschreibung>Eine Liste der Kommunikationsmedien und -kanäle, über die man eine Person oder
        Institution erreichen kann.</dat:beschreibung>
    <dat:herausgebernameLang>Koordinierungsstelle für IT-Standards</dat:herausgebernameLang>
    <dat:herausgebernameKurz>KoSIT</dat:herausgebernameKurz>
    <dat:zeitpunktLetzteBearbeitung>2018-11-21T12:41:44.780Z</dat:zeitpunktLetzteBearbeitung>
    <dat:zeitpunktAnlage>2014-07-17T11:42:07+02:00</dat:zeitpunktAnlage>
    <dat:statusVeroeffentlichung>
        <code>VEROEFFENTLICHT</code>
    </dat:statusVeroeffentlichung>
    <dat:statusVerwendung>
        <code>AKTUELL</code>
    </dat:statusVerwendung>
    <dat:aktualisierungszyklus>
        <code>UNREGELMAESSIGE_AKTUALISIERUNG</code>
    </dat:aktualisierungszyklus>
    <dat:besitzer>
        .
        .
        .
    </dat:ansprechpartner>
    <dat:versionCodeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit_1</dat:versionCodeliste.kennung>
    <dat:versionCodeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit_2</dat:versionCodeliste.kennung>
    <dat:versionCodeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit_3</dat:versionCodeliste.kennung>
    <dat:versionCodeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit_4</dat:versionCodeliste.kennung>
</dat:Codeliste>

```
## Metadaten einer Codelistenversion
Mit diesem Abruf werden die Metadaten zu einer konkreten Version abgefragt. Der Abrufparameter ist die Kennung der Codelistenversion. Geliefert werden alle versionsspezifischen Metadaten inklusive der bei der Einstellung ins XRepository erzeugten Validierungsinformationen.

**Request:** https://www.xrepository.de/api/version_codeliste/urn:de:xoev:codeliste:erreichbarkeit_3  

**Response:**
```
<dat:VersionCodeliste>
    <dat:kennung>urn:de:xoev:codeliste:erreichbarkeit_3</dat:kennung>
    <dat:version>3</dat:version>
    <dat:zeitpunktEinstellung>2018-05-25T14:41:25+02:00</dat:zeitpunktEinstellung>
    <dat:zeitpunktLetzteBearbeitung>2018-11-21T12:41:44.783Z</dat:zeitpunktLetzteBearbeitung>
    <dat:statusVeroeffentlichung>
        <code>VEROEFFENTLICHT</code>
    </dat:statusVeroeffentlichung>
    <dat:statusVerwendung>
        <code>AKTUELL</code>
    </dat:statusVerwendung>
    <dat:statusFassung>
        <code>ENDFASSUNG</code>
    </dat:statusFassung>
    <dat:datumGueltigkeitAb>2018-05-03</dat:datumGueltigkeitAb>
    <dat:versionXOEVHandbuch>2.1</dat:versionXOEVHandbuch>
    <dat:aenderungZurVorversion>Der Code 09 mit der Beschreibung "Web" wurde hinzugefügt. Die
        Beschreibung der Codeliste wurde angepasst, da die Codeliste auch zur Bestimmung der
        Erreichbarkeit von Institutionen genutzt werden kann. Konformität zu XÖV 2.1 wurde
        hergestellt. In diesem Rahmen wurden die Metadaten der Codeliste und ihrer Spalten
        konsolidiert und ergänzt.</dat:aenderungZurVorversion>
    <dat:codeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit</dat:codeliste.kennung>
    <dat:nutzendeVersionStandard.kennung>urn:xoev-de:kosit:standard:xgewerbeanzeige_2.0</dat:nutzendeVersionStandard.kennung>
    <dat:technischerBestandteilGenericode>
        <dat:datei>
            <dat:dateiname>Erreichbarkeit_3.xml</dat:dateiname>
            <dat:mimeType>application/xml</dat:mimeType>
            <dat:dateiFormat>xml</dat:dateiFormat>
        </dat:datei>
    </dat:technischerBestandteilGenericode>
    <dat:dokument>
        <dat:name>Codeliste im Excel-Format (xlsx)</dat:name>
        <dat:beschreibung>Codeliste im Excel-Format (xlsx)</dat:beschreibung>
        <dat:dokumentenkategorie>
            <code>WEITERE_DOKUMENTATION</code>
        </dat:dokumentenkategorie>
        <dat:statusVeroeffentlichung>
            <code>VEROEFFENTLICHT</code>
        </dat:statusVeroeffentlichung>
        <dat:zeitpunktLetzteBearbeitung>2018-05-25T14:48:41+02:00</dat:zeitpunktLetzteBearbeitung>
        <dat:datei>
            <dat:dateiname>Erreichbarkeit_3.zip</dat:dateiname>
            <dat:mimeType>application/x-zip-compressed</dat:mimeType>
            <dat:dateiFormat>ZIP</dat:dateiFormat>
            <dat:xrepository2Kennung>urn:uuid:405b24f0-f30d-424d-bd0e-bf527816b6e7</dat:xrepository2Kennung>
        </dat:datei>
    </dat:dokument>
    <ext:versionCodelisteExtrakt>

        .
        .
        .

    </ext:versionCodelisteExtrakt>
    <svrl:schematron-output schemaVersion="1.0.0" title="">
        <svrl:ns-prefix-in-attribute-values prefix="gc_1_0"
            uri="http://docs.oasis-open.org/codelist/ns/genericode/1.0/"/>
        <svrl:ns-prefix-in-attribute-values prefix="xoev-cl"
            uri="http://xoev.de/schemata/genericode/1_0"/>
        <svrl:ns-prefix-in-attribute-values prefix="xoev-cl-2"
            uri="http://xoev.de/schemata/genericode/2"/>
        <svrl:active-pattern documents="" id="pattern.xoev_version_check"
            name="pattern.xoev_version_check"/>
        <svrl:active-pattern documents="" id="pattern.gc_1_0.rule_001"
            name="pattern.gc_1_0.rule_001"/>

        .
        .
        .

    </svrl:schematron-output>
    <dat:xrepository2Kennung>urn:de:xoev:codeliste:erreichbarkeit_3</dat:xrepository2Kennung>
</dat:VersionCodeliste>
```
## Aktuell gültige Version einer Codeliste
Mit diesem Abruf wird die aktuell gültige Version einer im XRepository gehaltene Codeliste abgefragt. Abrufparameter sind die Kennung der Codeliste und der URL-Parameter "/gueltigeVersion". Geliefert werden die Metadaten der aktuell gültigen Version der Codeliste.
Aktuell gültig sind Versionen von Codelisten die

- die Status "aktuell", "Endfassung" und "veröffentlicht" innehaben und
- deren Gültigkeit-ab-Datum (/gc:CodeList/Annotation/Description/xoev-cl-2:datumGueltigkeitAb) kleiner ist als das aktuelle Datum.
- deren Gültigkeit-bis-Datum (kann nur über die GUI des XRepository gesetzt werden) größer ist als das aktuelle Datum.

Falls zur abgefragten Codeliste keine Versionen existieren, die die genannten Kriterien erfüllen wird eine entsprechende Fehlermeldung zurück gegeben.
Existieren mehrere Versionen, werden die Metadaten der Liste mit dem "jüngsten" Gültigkeit-ab-Datum zurück geliefert.
Existieren mehrere Versionen der Codeliste mit gleichen Gültigkeit-ab-Datum, werden die Metadaten der Liste mit dem "jüngsten" Einstelldatum zurückgelefert.

Der Response dieses Abrufs ist identisch zum oben dargestellten Reponse des Abrufs der Metadaten einer Codelistenversion.

**Request:** https://www.xrepository.de/api/codeliste/urn:de:xoev:codeliste:erreichbarkeit/gueltigeVersion  

**Response:**
```
<dat:VersionCodeliste>
    <dat:kennung>urn:de:xoev:codeliste:erreichbarkeit_3</dat:kennung>
    <dat:version>3</dat:version>
    <dat:zeitpunktEinstellung>2018-05-25T14:41:25+02:00</dat:zeitpunktEinstellung>
    <dat:zeitpunktLetzteBearbeitung>2018-11-21T12:41:44.783Z</dat:zeitpunktLetzteBearbeitung>
    <dat:statusVeroeffentlichung>
        <code>VEROEFFENTLICHT</code>
    </dat:statusVeroeffentlichung>
    <dat:statusVerwendung>
        <code>AKTUELL</code>
    </dat:statusVerwendung>
    <dat:statusFassung>
        <code>ENDFASSUNG</code>
    </dat:statusFassung>
    <dat:datumGueltigkeitAb>2018-05-03</dat:datumGueltigkeitAb>
    <dat:versionXOEVHandbuch>2.1</dat:versionXOEVHandbuch>
    <dat:aenderungZurVorversion>Der Code 09 mit der Beschreibung "Web" wurde hinzugefügt. Die
        Beschreibung der Codeliste wurde angepasst, da die Codeliste auch zur Bestimmung der
        Erreichbarkeit von Institutionen genutzt werden kann. Konformität zu XÖV 2.1 wurde
        hergestellt. In diesem Rahmen wurden die Metadaten der Codeliste und ihrer Spalten
        konsolidiert und ergänzt.</dat:aenderungZurVorversion>
    <dat:codeliste.kennung>urn:de:xoev:codeliste:erreichbarkeit</dat:codeliste.kennung>
    <dat:nutzendeVersionStandard.kennung>urn:xoev-de:kosit:standard:xgewerbeanzeige_2.0</dat:nutzendeVersionStandard.kennung>
    <dat:technischerBestandteilGenericode>
        <dat:datei>
            <dat:dateiname>Erreichbarkeit_3.xml</dat:dateiname>
            <dat:mimeType>application/xml</dat:mimeType>
            <dat:dateiFormat>xml</dat:dateiFormat>
        </dat:datei>
    </dat:technischerBestandteilGenericode>
    <dat:dokument>
        <dat:name>Codeliste im Excel-Format (xlsx)</dat:name>
        <dat:beschreibung>Codeliste im Excel-Format (xlsx)</dat:beschreibung>
        <dat:dokumentenkategorie>
            <code>WEITERE_DOKUMENTATION</code>
        </dat:dokumentenkategorie>
        <dat:statusVeroeffentlichung>
            <code>VEROEFFENTLICHT</code>
        </dat:statusVeroeffentlichung>
        <dat:zeitpunktLetzteBearbeitung>2018-05-25T14:48:41+02:00</dat:zeitpunktLetzteBearbeitung>
        <dat:datei>
            <dat:dateiname>Erreichbarkeit_3.zip</dat:dateiname>
            <dat:mimeType>application/x-zip-compressed</dat:mimeType>
            <dat:dateiFormat>ZIP</dat:dateiFormat>
            <dat:xrepository2Kennung>urn:uuid:405b24f0-f30d-424d-bd0e-bf527816b6e7</dat:xrepository2Kennung>
        </dat:datei>
    </dat:dokument>
    <ext:versionCodelisteExtrakt>

        .
        .
        .

    </ext:versionCodelisteExtrakt>
    <svrl:schematron-output schemaVersion="1.0.0" title="">
        <svrl:ns-prefix-in-attribute-values prefix="gc_1_0"
            uri="http://docs.oasis-open.org/codelist/ns/genericode/1.0/"/>
        <svrl:ns-prefix-in-attribute-values prefix="xoev-cl"
            uri="http://xoev.de/schemata/genericode/1_0"/>
        <svrl:ns-prefix-in-attribute-values prefix="xoev-cl-2"
            uri="http://xoev.de/schemata/genericode/2"/>
        <svrl:active-pattern documents="" id="pattern.xoev_version_check"
            name="pattern.xoev_version_check"/>
        <svrl:active-pattern documents="" id="pattern.gc_1_0.rule_001"
            name="pattern.gc_1_0.rule_001"/>

        .
        .
        .

    </svrl:schematron-output>
    <dat:xrepository2Kennung>urn:de:xoev:codeliste:erreichbarkeit_3</dat:xrepository2Kennung>
</dat:VersionCodeliste>
```
## Genericode-Datei einer Codelistenversion
Mit dem folgenden Abruf wird der Parameter Versionskennung übergeben und die entsprechende Version der Codeliste im Genericode-Format zurückgeliefert. Ein alternativer Abruf für die Abfrage der Codeliste in JSON-Format wird testweise geoboten.

**Request:** https://www.xrepository.de/api/version_codeliste/urn:de:xoev:codeliste:erreichbarkeit_3/genericode  

**alternativer Request:** https://www.xrepository.de/api/version_codeliste/urn:de:xoev:codeliste:erreichbarkeit_3/genericode-daten

**Response:**
```
<gc:CodeList>
    <Annotation>
        <Description>
            <!--Metadaten entsprechend XÖV-Handbuch 2.0-->
            <xoev-cl:beschreibung>Eine Liste der Kommunikationsmedien und -kanäle, über die man eine
                Person oder Institution erreichen kann.</xoev-cl:beschreibung>
            <xoev-cl:datumgueltigkeitab>2018-09-03</xoev-cl:datumgueltigkeitab>
            <!--Metadaten entsprechend XÖV-Handbuch 2.1-->
            <xoev-cl-2:nameKurz>Erreichbarkeit</xoev-cl-2:nameKurz>
            <xoev-cl-2:beschreibung-codeliste>Eine Liste der Kommunikationsmedien und -kanäle, über
                die man eine Person oder Institution erreichen
                kann.</xoev-cl-2:beschreibung-codeliste>
            <xoev-cl-2:herausgebernameKurz>KoSIT</xoev-cl-2:herausgebernameKurz>
            <xoev-cl-2:datumGueltigkeitAb>2018-09-03</xoev-cl-2:datumGueltigkeitAb>
            <xoev-cl-2:versionXOEVHandbuch>2.1</xoev-cl-2:versionXOEVHandbuch>
            <xoev-cl-2:aenderungZurVorversion>Der Code 09 mit der Beschreibung "Web" wurde
                hinzugefügt. Die Beschreibung der Codeliste wurde angepasst, da die Codeliste auch
                zur Bestimmung der Erreichbarkeit von Institutionen genutzt werden kann. Konformität
                zu XÖV 2.1 wurde hergestellt. In diesem Rahmen wurden die Metadaten der Codeliste
                und ihrer Spalten konsolidiert und ergänzt.</xoev-cl-2:aenderungZurVorversion>
        </Description>
    </Annotation>
    <Identification>
        <ShortName>erreichbarkeit</ShortName>
        <LongName>Kommunikationskanal / Erreichbarkeit</LongName>
        <Version>3</Version>
        <CanonicalUri>urn:de:xoev:codeliste:erreichbarkeit</CanonicalUri>
        <CanonicalVersionUri>urn:de:xoev:codeliste:erreichbarkeit_3</CanonicalVersionUri>
        <Agency>
            <ShortName>KoSIT</ShortName>
            <LongName>Koordinierungsstelle für IT-Standards</LongName>
        </Agency>
    </Identification>
    <ColumnSet>
        <Column Id="code" Use="required">
            <ShortName>code</ShortName>
            <LongName>Code</LongName>
            <Data Type="string"/>
        </Column>
        <Column Id="Beschreibung" Use="required">
            <ShortName>Beschreibung</ShortName>
            <LongName>Beschreibung</LongName>
            <Data Type="string"/>
        </Column>
        <Column Id="iana-uri-scheme" Use="optional">
            <ShortName>iana-uri-scheme</ShortName>
            <LongName>IANA Uniform Resource Identifier (URI) Scheme</LongName>
            <Data Type="string"/>
        </Column>
        <Key Id="codeKey">
            <Annotation>
                <AppInfo>
                    <xoev-cl:xoev-code/>
                    <xoev-cl-2:xoev-code/>
                </AppInfo>
            </Annotation>
            <ShortName>codeKey</ShortName>
            <ColumnRef Ref="code"/>
        </Key>
    </ColumnSet>
    <SimpleCodeList>
        <Row>
            <Value ColumnRef="code">
                <SimpleValue>01</SimpleValue>
            </Value>
            <Value ColumnRef="Beschreibung">
                <SimpleValue>E-Mail</SimpleValue>
            </Value>
            <Value ColumnRef="iana-uri-scheme">
                <SimpleValue>mailto</SimpleValue>
            </Value>
        </Row>
        .
        .
        .
        <Row>
            <Value ColumnRef="code">
                <SimpleValue>10</SimpleValue>
            </Value>
            <Value ColumnRef="Beschreibung">
                <SimpleValue>nicht erreichbar</SimpleValue>
            </Value>
            <Value ColumnRef="iana-uri-scheme">
                <SimpleValue>optional</SimpleValue>
            </Value>
        </Row>
    </SimpleCodeList>
</gc:CodeList>

```

# Übersicht nicht mehr unterstützter SOAP-basierten Webservices zu Codelisten
## Versionen einer Codeliste abrufen
Mit diesem Abbruf wurden die im XRepository gehaltene Versionen einer Codeliste abgefragt. Der Abrufparameter ist die Kennung der Codeliste. Geliefert wird das Metadatum Version (/gc:CodeList/Identification/Version) der zugehörigen Codelistenversionen im Status "veröffentlicht".  
**Request:**
```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xrep="https://www.xrepository.de/services">
	<soapenv:Header/>
	<soapenv:Body>
		<xrep:getVersionNamesFromCodelist>
			<xrep:uri>urn:de:xoev:codeliste:erreichbarkeit</xrep:uri>
		</xrep:getVersionNamesFromCodelist>
	</soapenv:Body>
</soapenv:Envelope>
```
**Response:**
```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <ns2:getVersionNamesFromCodelistResponse xmlns:ns2="https://www.xrepository.de/services"
            xmlns:ns3="http://docs.oasis-open.org/codelist/ns/genericode/1.0/">
            <ns3:Version>3</ns3:Version>
            <ns3:Version>2</ns3:Version>
            <ns3:Version>1</ns3:Version>
        </ns2:getVersionNamesFromCodelistResponse>
    </soap:Body>
</soap:Envelope>
```
### Aktuell gültige Version einer Codeliste abrufen
**Request:**
```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xrep="https://www.xrepository.de/services">
	<soapenv:Header/>
	<soapenv:Body>
		<xrep:getCurrentVersionNameFromCodelist>
			<xrep:uri>urn:de:xoev:codeliste:erreichbarkeit</xrep:uri>
		</xrep:getCurrentVersionNameFromCodelist>
	</soapenv:Body>
</soapenv:Envelope>
```
**Response:**
```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <ns2:getCurrentVersionNameFromCodelistResponse
            xmlns:ns2="https://www.xrepository.de/services"
            xmlns:ns3="http://docs.oasis-open.org/codelist/ns/genericode/1.0/">
            <ns3:Version>3</ns3:Version>
        </ns2:getCurrentVersionNameFromCodelistResponse>
    </soap:Body>
</soap:Envelope>
```
### Codeliste (Genericode-Datei) abrufen
**Request:**
```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xrep="https://www.xrepository.de/services">
	<soapenv:Header/>
	<soapenv:Body>
		<xrep:getGenericode>
			<xrep:uri>urn:de:xoev:codeliste:erreichbarkeit</xrep:uri>
			<xrep:version>3</xrep:version>
		</xrep:getGenericode>
	</soapenv:Body>
</soapenv:Envelope>
```
**Response:**
```
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <ns2:getGenericodeResponse xmlns:ns2="https://www.xrepository.de/services"
            xmlns:ns3="http://docs.oasis-open.org/codelist/ns/genericode/1.0/">
            <ns3:CodeList>
                <Annotation>
                    <Description>
                        <xoev-cl:beschreibung>Eine Liste der
                            Kommunikationsmedien und -kanäle, über die man eine Person oder
                            Institution erreichen kann.</xoev-cl:beschreibung>
                        <xoev-cl:datumgueltigkeitab>2018-05-03</xoev-cl:datumgueltigkeitab>
                        <xoev-cl-2:nameKurz>Erreichbarkeit</xoev-cl-2:nameKurz>
                        <xoev-cl-2:beschreibung-codeliste>Eine Liste der
                            Kommunikationsmedien und -kanäle, über die man eine Person oder
                            Institution erreichen kann.</xoev-cl-2:beschreibung-codeliste>
                        <xoev-cl-2:herausgebernameKurz>KoSIT</xoev-cl-2:herausgebernameKurz>
                        <xoev-cl-2:datumGueltigkeitAb>2018-05-03</xoev-cl-2:datumGueltigkeitAb>
                        <xoev-cl-2:versionXOEVHandbuch>2.1</xoev-cl-2:versionXOEVHandbuch>
                        <xoev-cl-2:aenderungZurVorversion>Der Code 09 mit
                            der Beschreibung "Web" wurde hinzugefügt. Die Beschreibung der Codeliste
                            wurde angepasst, da die Codeliste auch zur Bestimmung der Erreichbarkeit
                            von Institutionen genutzt werden kann. Konformität zu XÖV 2.1 wurde
                            hergestellt. In diesem Rahmen wurden die Metadaten der Codeliste und
                            ihrer Spalten konsolidiert und
                            ergänzt.</xoev-cl-2:aenderungZurVorversion>
                    </Description>
                </Annotation>
                <Identification>
                    <ShortName>erreichbarkeit</ShortName>
                    <LongName>Kommunikationskanal / Erreichbarkeit</LongName>
                    <Version>3</Version>
                    <CanonicalUri>urn:de:xoev:codeliste:erreichbarkeit</CanonicalUri>
                    <CanonicalVersionUri>urn:de:xoev:codeliste:erreichbarkeit_3</CanonicalVersionUri>
                    <Agency>
                        <ShortName>KoSIT</ShortName>
                        <LongName>Koordinierungsstelle für IT-Standards</LongName>
                    </Agency>
                </Identification>
                <ColumnSet>
                    <Column Use="required" Id="code">
                        <ShortName>code</ShortName>
                        <LongName>Code</LongName>
                        <Data Type="string"/>
                    </Column>
                    <Column Use="required" Id="Beschreibung">
                        <ShortName>Beschreibung</ShortName>
                        <LongName>Beschreibung</LongName>
                        <Data Type="string"/>
                    </Column>
                    <Column Use="optional" Id="iana-uri-scheme">
                        <ShortName>iana-uri-scheme</ShortName>
                        <LongName>IANA Uniform Resource Identifier (URI) Scheme</LongName>
                        <Data Type="string"/>
                    </Column>
                    <Key Id="codeKey">
                        <Annotation>
                            <AppInfo>
                                <xoev-cl:xoev-code/>
                                <xoev-cl-2:xoev-code/>
                            </AppInfo>
                        </Annotation>
                        <ShortName>codeKey</ShortName>
                        <ColumnRef Ref="code"/>
                    </Key>
                </ColumnSet>
                <SimpleCodeList>
                    <Row>
                        <Value ColumnRef="code">
                            <SimpleValue>01</SimpleValue>
                        </Value>
                        <Value ColumnRef="Beschreibung">
                            <SimpleValue>E-Mail</SimpleValue>
                        </Value>
                        <Value ColumnRef="iana-uri-scheme">
                            <SimpleValue>mailto</SimpleValue>
                        </Value>
                    </Row>
                    .
                    .
                    .                    
                    <Row>
                        <Value ColumnRef="code">
                            <SimpleValue>09</SimpleValue>
                        </Value>
                        <Value ColumnRef="Beschreibung">
                            <SimpleValue>Web</SimpleValue>
                        </Value>
                        <Value ColumnRef="iana-uri-scheme">
                            <SimpleValue>http, https</SimpleValue>
                        </Value>
                    </Row>
                </SimpleCodeList>
            </ns3:CodeList>
        </ns2:getGenericodeResponse>
    </soap:Body>
</soap:Envelope>

```
